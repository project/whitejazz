<?php

/**
 * @file
 * WhiteJazz theme_settings.php
 *
 */

function whitejazz_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

  $unit_options = array('%' => '%', 'px' => 'px', 'em' => 'em');

  $form['page'] = array(
    '#type' => 'details',
    '#title' => t('Page settings'),
    '#open' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['font'] = array(
    '#type' => 'details',
    '#title' => t('Font settings'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['css_file'] = array(
    '#type' => 'details',
    '#title' => t('Custom css settings'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['misc'] = array(
    '#type' => 'details',
    '#title' => t('Misc settings'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['page']['whitejazz_page_width_unit'] = array(
    '#type' => 'select',
    '#title' => t('Page width unit'),
    '#default_value' => theme_get_setting('whitejazz_page_width_unit', $theme = NULL),
    '#options' => $unit_options,
  );

  $form['page']['whitejazz_page_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width Size'),
    '#description' => t('Set the width. You  can set the width in percent for dynamic width or in px for fixed width.<br /><b>eg.: 850px or 60%</b>'),
    '#default_value' => theme_get_setting('whitejazz_page_width', $theme = NULL),
    '#size' => 7,
    '#maxlength' => 7,
  );

  $form['page']['whitejazz_page_max_width_unit'] = array(
    '#type' => 'select',
    '#title' => t('Page max-width unit'),
    '#default_value' => theme_get_setting('whitejazz_page_max_width_unit', $theme = NULL),
    '#options' => $unit_options,
  );

  $form['page']['whitejazz_page_max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Max width Size'),
    '#description' => t('Set the width. You  can set the width in percent for dynamic width or in px for fixed width.<br /><b>eg.: 850px or 60%</b>'),
    '#default_value' => theme_get_setting('whitejazz_page_max_width', $theme = NULL),
    '#size' => 7,
    '#maxlength' => 7,
  );

  $form['page']['whitejazz_sidebar_width_unit'] = array(
    '#type' => 'select',
    '#title' => t('Sidebar width unit'),
    '#default_value' => theme_get_setting('whitejazz_sidebar_width_unit', $theme = NULL),
    '#options' => $unit_options,
  );

  $form['page']['whitejazz_sidebar_first_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Left Sidebar Width'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('whitejazz_sidebar_first_width', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['page']['whitejazz_sidebar_second_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Right Sidebar Width'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('whitejazz_sidebar_second_width', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['font']['whitejazz_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#description' => t('Choose your favorite Fonts'),
    '#default_value' => theme_get_setting('whitejazz_font_family', $theme = NULL),
    '#options' => array(
      'Arial, Verdana, sans-serif' => t('Arial, Verdana, sans-serif'),
      '"Arial Narrow", Arial, Helvetica, sans-serif' => t('"Arial Narrow", Arial, Helvetica, sans-serif'),
      '"Times New Roman", Times, serif' => t('"Times New Roman", Times, serif'),
      '"Lucida Sans", Verdana, Arial, sans-serif' => t('"Lucida Sans", Verdana, Arial, sans-serif'),
      '"Lucida Grande", Verdana, sans-serif' => t('"Lucida Grande", Verdana, sans-serif'),
      'Tahoma, Verdana, Arial, Helvetica, sans-serif' => t('Tahoma, Verdana, Arial, Helvetica, sans-serif'),
      'Georgia, "Times New Roman", Times, serif' => t('Georgia, "Times New Roman", Times, serif'),
      'Custom' => t('Custom (specify below)'),
    ),
  );

  $form['font']['whitejazz_custom_font'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Font-Family Setting'),
    '#description' => t('type your fonts separated by ,<br />eg. <b>"Lucida Grande", Verdana, sans-serif</b>'),
    '#default_value' => theme_get_setting('whitejazz_custom_font', $theme = NULL),
    '#size' => 40,
    '#maxlength' => 75,
  );

  $form['css_file']['whitejazz_use_local_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use custom Stylesheet'),
    '#description' => t('Use the css code below'),
    '#default_value' => theme_get_setting('whitejazz_use_local_content', $theme = NULL),
  );

  $form['css_file']['whitejazz_local_content_file'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom Stylesheet'),
    '#description' => t('type your custom css code here'),
    '#default_value' => theme_get_setting('whitejazz_local_content_file', $theme = NULL),
//    '#size' => 40,
//    '#maxlength' => 75,
  );

  $form['misc']['whitejazz_use_custom_logo_size'] = array(
    '#type' => 'checkbox',
    '#title' => t('Specify Custom Logo Size'),
    '#default_value' => theme_get_setting('whitejazz_use_custom_logo_size', $theme = NULL),
  );

  $form['misc']['whitejazz_logo_width_unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit'),
    '#default_value' => theme_get_setting('whitejazz_logo_width_unit', $theme = NULL),
    '#options' => $unit_options,
  );

  $form['misc']['whitejazz_logowidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo Width'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('whitejazz_logo_width', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['misc']['whitejazz_logoheight'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo Height'),
    '#default_value' => theme_get_setting('whitejazz_logo_height', $theme = NULL),
    '#description' => t('Set the width in Pixel.'),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['misc']['whitejazz_roople_footer_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show RoopleTheme footer logo'),
    '#description' => t('if unchecked then roopletheme logo in the footer will disapear<br />so you don\'t need touch the code'),
    '#default_value' => theme_get_setting('whitejazz_roople_footer_logo', $theme = NULL),
  );
  //return $form;
}

